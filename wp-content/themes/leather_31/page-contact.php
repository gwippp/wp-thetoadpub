<?php get_header(); ?>
	<main class="single contact col col-sm-6 col-md-6 col-lg-5">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
			<h2><?php the_title(); ?></h2>
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
			
		<?php endwhile; endif; ?>
		<?php edit_post_link('Edit', '<p>', '</p>'); ?>
	</main>
	<div class="col-sm-6 col-md-6 col-lg-7">
		<br><?php include (TEMPLATEPATH . "/components/map.php"); ?>
	</div>
	<?php // get_sidebar(); ?>
<?php get_footer(); ?>
