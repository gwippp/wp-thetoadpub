<?php get_template_part('html-head'); ?>
<div class="container">
	<div class="row">
		<header role="banner" class="col col-xs-12">
			<h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<h2><?php bloginfo('description'); ?></h2>
			<nav id="main_menu" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
			<nav id="social_menu" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
			<?php query_posts(array('posts_per_page' => 1, 'cat' => '6', 'orderby' => 'rand'));
				while (have_posts()) : the_post();
				global $post; ?>
					<blockquote cite="<?php the_title(); ?>">
						&ldquo;<?php echo get_the_content(); ?>&rdquo; - <?php the_title(); ?>
					</blockquote>
				<?php endwhile;
			wp_reset_query(); ?>
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
		</header>
	</div>
	<div class="row">
