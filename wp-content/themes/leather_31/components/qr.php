<div id="qr">
	<span>Az alábbi <strong>QR</strong> kód a névjegyünket tartalmazza, a legfontosabb információkkal, amennyiben rendelkezik okostelefonnal, olvassa be, és elérhetőségi adataink mindíg kéznél lesznek.</span>
	<img src="<?php bloginfo('template_url'); ?>/img/qrcode.png" alt="qr code" />
</div>
