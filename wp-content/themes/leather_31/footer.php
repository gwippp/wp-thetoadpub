	</div>
	<div class="row">
		<footer id="footer" role="contentinfo" class="col col-xs-12">
			<div class="container">
				<div class="row">
					<nav id="footer_menu" role="navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'nav-menu' ) ); ?>
					</nav>
					<p id="copy_text">© Copyright - The Toad in the Hole Pub <?php echo date("Y"); ?></p>
				</div>
			</div>
		</footer>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
