<aside  class="<?php echo($asideColClasses); ?> col col-md-5 col-lg-3">
	<section>
		<nav id="sidebar_menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'sidebar', 'menu_class' => 'nav-menu' ) ); ?>
		</nav>
	</section>
</aside>
